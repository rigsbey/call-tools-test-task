import pytest
import logging
from main import (
    fetch_articles_without_comments,
    process_work_hours,
    read_test_cases,
)


@pytest.fixture
def work_hours_data():
    return """
    Андрей 9
    Василий 11
    Роман 7
    X Æ A-12 45
    Иван Петров 3
    Андрей 6
    Роман 11
    """


def test_fetch_articles_without_comments(mocker):
    mock_cursor = mocker.patch("main.psycopg2.connect")
    fetch_articles_without_comments()
    mock_cursor.assert_called_once()


def test_process_work_hours(caplog, work_hours_data):
    with caplog.at_level(logging.INFO):
        process_work_hours(work_hours_data)
    assert "Андрей: 9, 6; sum: 15" in caplog.text
    assert "Василий: 11; sum: 11" in caplog.text
    assert "Роман: 7, 11; sum: 18" in caplog.text
    assert "X Æ A-12: 45; sum: 45" in caplog.text
    assert "Иван Петров: 3; sum: 3" in caplog.text


def test_read_test_cases(tmpdir):
    test_file = tmpdir.join("test_cases.txt")
    test_file.write(
        """
Андрей 9
Василий 11

Роман 7
X Æ A-12 45
    """
    )
    test_cases = read_test_cases(str(test_file))
    assert len(test_cases) == 2
    assert "Андрей 9\nВасилий 11" in test_cases
    assert "Роман 7\nX Æ A-12 45" in test_cases
