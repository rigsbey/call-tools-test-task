import logging
import os
from collections import defaultdict

import psycopg2

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def get_db_connection():
    return psycopg2.connect(
        dbname=os.getenv("DB_NAME", "test_task"),
        user=os.getenv("DB_USER", "postgres"),
        password=os.getenv("DB_PASSWORD", "postgres"),
        host=os.getenv("DB_HOST", "localhost"),
        port=os.getenv("DB_PORT", "5432"),
    )


def fetch_articles_without_comments():
    try:
        conn = get_db_connection()
        cursor = conn.cursor()
        query = """
                SELECT a.id, a.title, a.text
                FROM article a
                LEFT JOIN comment c ON a.id = c.article_id
                WHERE c.id IS NULL;
                """
        cursor.execute(query)
        articles = cursor.fetchall()
    except psycopg2.DatabaseError as e:
        logging.error(f"Ошибка при выполнении запроса: {e}")
        articles = []
    finally:
        cursor.close()
        conn.close()
    return articles


def process_work_hours(input_data):
    hours = defaultdict(list)
    for line in input_data.strip().split("\n"):
        name, hour = line.rsplit(" ", 1)
        hours[name.strip()].append(int(hour))
    for name, hour_list in hours.items():
        sum_hours = sum(hour_list)
        hour_list_str = ", ".join(map(str, hour_list))
        logging.info(f"{name}: {hour_list_str}; sum: {sum_hours}")


def read_test_cases(file_path):
    with open(file_path, "r", encoding="utf-8") as file:
        data = file.read().strip()
    return data.split("\n\n")


def main():
    articles_without_comments = fetch_articles_without_comments()
    if articles_without_comments:
        logging.info("Статьи без комментариев:")
        for article in articles_without_comments:
            logging.info(f"ID: {article[0]}, Title: {article[1]}, Text: {article[2]}")
    else:
        logging.info("Нет статей без комментариев.")

    test_cases = read_test_cases("test_cases.txt")
    for i, case in enumerate(test_cases, 1):
        logging.info(f"Тестовый кейс {i}:")
        process_work_hours(case)


if __name__ == "__main__":
    main()
